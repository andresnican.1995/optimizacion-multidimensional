%% M�todo de NEWTON-RAPHSON

%Comandos Iniciales para limpiar
clear,  clc,      close all 
%Ingreso la funci�n y lo almaceno en la variable cf (cadena funci�n)
cf = input('Ingrese la funci�n a evaluar: ');
% Declaro x como una variable simbolica. 
syms x 
% tranformo la cadena en una funci�n inline
f = inline(cf);
%Obtenemos la derifava con la cadena de la funci�n
derivada = diff(cf, x);
df = inline(derivada);
tol = input('Ingrese la tolerancia: ');
%error grande para acceder al while
error = 50;
x = input('Ingrese un valor inicial: ');
%inicio el numero de interacciones en 0
n=0;
%Encabezado que voy a mostrar
disp('    n      x1     error')
%iniciamos el while e indicamos que siga interando mientras el error sea
%mayor a al tolerancia.
while(error>tol)
    %Para mostrar la funci�n 
    %   \t es dejar un tabulador
    %   %i = fomato para mostrar enteros
    %   %3.5f  3 enteros 5 decimales f coma flotante    
    fprintf('\t%i\t%3.5f\t%f\n', n, x, error);
    
    n = n+1;
    
    %Calculamos un nuevo valor de x cin cada interaci�n
    % x se actualiza al valor anterior
    x = x - f(x)/df(x);
    error = abs( f(x) );
end